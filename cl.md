---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Cátedra Libre
subtitle: índice con el material
layout: default
---

Reuniones remotas: [https://meet.calyx.net/CLST_FCH_UNRC](https://meet.calyx.net/CLST_FCH_UNRC)

Lista de correo: próximamente

## Documentación
- [Resolución 491/2014 FCH: creación de cátedras abiertas](/docs/RCD-2014-0491-E-HUM.pdf)
- [Plan Institucional FCH](/docs/Plan_Institucional-FCH-UNRC.pdf)
