---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Chocancharava Libre
subtitle: Articulando Procesos Comunitarios
layout: default
hero_height: large
---

{: .mt-0 .has-text-centered }
> Las redes comunitarias son redes construidas de forma colaborativa y de abajo hacia arriba por grupos de individuos que desarrollan y gestionan nuevas infraestructuras de red como <b>bienes comunes</b>.

{: .title .is-2 }
## Definición de red comunitaria

{: .has-text-justified }
Las redes comunitarias son redes de propiedad y gestión colectiva de la comunidad, sin finalidad de lucro y con fines comunitarios.<br>
Se constituyen como colectivos, comunidades indígenas u organizaciones de la sociedad civil sin fines de lucro, que ejercen su derecho a la comunicación, bajo principios de participación democrática de sus miembros, equidad, igualdad de género, diversidad y pluralidad.<br>
La información sobre el diseño y funcionamiento es abierta y accesible, permitiendo y favoreciendo la extensión de la red por parte de los usuarios. Las redes comunitarias fomentan los servicios y contenidos locales, promueven la neutralidad de la red y la celebración de acuerdos de interconexión y tránsito libres y gratuitos con las redes que ofrecen reciprocidad.<br><br>
<i>Definición establecida por la Declaración de la primera Cumbre Latinoamericana de Redes Comunitarias, septiembre de 2018.</i>

{: .has-text-centered }
> Articulamos con Organizaciones territoriales locales interesadas en generar procesos de trabajos.


{: .has-text-centered }
> [**AlterMundi**](https://altermundi.net) nos acompaña en este andar.
