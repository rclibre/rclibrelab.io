---
title: Conversatorio
subtitle: Redes comunitarias de acceso a Internet en el entorno rural
layout: conversatorio
hero_height: large
---    

### Resumen
> Compartiremos experiencias y saberes para conocer proyectos exitosos en el despliegue de redes comunitarias rurales, orientadas a brindar servicios, entre otros, a pequeños productores locales.
> Invitamos a integrantes de la Comunidad Trabajo Organización (CTO), que es parte de la Unión de Trabajadores Rurales (UTR - Córdoba), departamentos de San Alberto y San Javier, traslasierra con su Red Comunitaria "El Valle Reinicia" y la RC "NonoLibre" a charlar sobre sus trabajos ya desarrollados y planes futuros.

#### Despliegue de redes comunitarias en el Valle de Traslasierra. NonoLibre y El Valle reinicia (CTO)
- Proyecto ganador del programa FRIDA convocatoria 2020.
- El objetivo del proyecto es conectar con infraestructura de red inalámbrica a comunidades del valle de Traslasierra en Córdoba, Argentina, donde el acceso a internet es escaso o nulo. El proyecto completará un despliegue de infraestructura iniciado años atrás con el apoyo de Internet Society en los departamentos de San Alberto y San Javier.
[LEER MÁS](https://programafrida.net/archivos/project/red-traslasierra)

### Video
<div class="video-container">
  <iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/jJPO9pyI3Ic" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### Fecha y hora
- viernes 11/09/20, 10hs (hr de ARG)
- Duración aproximada: 90 min

### Participan
- Sol Talia Tejeda y Fabricio Puzio
  - Red Comunitaria El Valle Reinicia (Traslasierra)
  - Comunidad Trabajo Organización (CTO)
  - Unión de Trabajadores Rurales (UTR - Córdoba)
- Carmelo Valmala
  - Red Comunitaria NonoLibre (Nono)
- Melina Giudice y Nicolás Pace
  - AlterMundi
- Patricia Silva y Patricia Skejich
  - UNR / CIAP
- Daniel Bellomo
  - AlterMundi / CIAP / UNRC

### Coordinan
- Patricia Silva, Patricia Skejich y Daniel Bellomo

### Comunicación
- María Emilia Andreani (CIAP)

### Organizan
- Centro de Información de Actividades Porcinas - [CIAP](http://www.ciap.org.ar)
- Proyecto de Vinculación Tecnológica y Desarrollo Productivo, Facultad de Ciencias Agrarias, Universidad Nacional de Rosario
  - "Vinculación entre actores del sistema agroalimentario porcino mediante el Centro de Información de Actividades Porcinas (CIAP)"
  - Directora: Patricia Silva
  - Codirectora: Patricia Skejich

<figure class="image is-square">
  <img src="/assets/img/conversatorio.RRCC.ruralidad.png">
</figure>

#### Objetivo principal de “Vinculación Inclusiva”, Secretaría de Vinculación Tecnológica y Desarrollo Productivo, Universidad Nacional de Rosario
> Tiene como objetivo principal vincular a la Universidad, a través de todos sus estamentos, con los actores locales involucrados en procesos productivos, generando respuestas y estrechando lazos de confianza y cooperación, promoviendo de esta forma una cultura innovadora y fortaleciendo el desarrollo territorial de la región.  
Los proyectos de Vinculación Tecnológica buscan así valorizar el conocimiento científico y comunitario, reconociendo demandas sociales existentes o latentes, formulando un conjunto de actividades coherentes con los objetivos planteados y movilizando recursos para su resolución.

#### Principios básicos
> Vinculación y asociatividad con empresas, municipios y comunas, e instituciones públicas y privadas del medio productivo local.
Generación de procesos de aprendizaje, integrando el conocimiento basado en la ciencia, con aquél basado en la práctica y la experiencia.  
Co-construcción de conocimiento que acompañe los procesos de cambio del sector productivo, priorizando las necesidades territoriales y la sostenibilidad en el tiempo.

<figure class="image">
  <img src="/assets/img/libre-router.jpg">
</figure>

<div class="columns ">
  <div class="column ">
  <figure class="image ">
    <img class="is-rounded" src="/assets/img/CTO.jpg">
  </figure>
  </div>
  <div class="column ">
    <figure class="image ">
      <img class="is-rounded" src="/assets/img/UTR.jpg">
    </figure>
  </div>
  <div class="column ">
    <figure class="image ">
      <img class="is-rounded" src="/assets/img/NonoLibre.jpg">
    </figure>
  </div>
  <div class="column ">
    <figure class="image ">
      <img class="is-rounded" src="/assets/img/AlterMundi.png">
    </figure>
  </div>
  <div class="column ">
    <figure class="image ">
      <img class="is-rounded" src="/assets/img/CIAP.jpeg">
    </figure>
  </div>

</div>

{: .has-text-centered }
> [**AlterMundi**](https://altermundi.net) nos acompaña en este andar
