---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Redes Comunitarias
subtitle: procesos que nos inspiran
layout: default
---

- [Altermundi](https://altermundi.net)
- Quintana Libre (Córdoba)
- [Atalaya Sur (Buenos Aires)](http://www.proyectocomunidad.com/atalaya-sur/)
- [Rhizomática (México)](https://www.rhizomatica.org)
- Moinho Mesh (Brasil)
- REDES AC (México)
- Colnodo (Colombia)
- Red Fusa Libre (Colombia)
- [Portal sem Porteiras (Brasil)](https://vimeo.com/326452546)
- [Telecomunicaciones Indígenas Comunitarias (México)](https://www.tic-ac.org)
