---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Paraje Las Lagunitas
subtitle: Red Comunitaria
layout: default
---

El viernes 13 de agosto viajamos - Guillermo y Daniel - al paraje Las Lagunitas (11 km al norte en línea recta desde Alpa Corral) a reunirnos con las familias del lugar para presentarles formalmente el proyecto de la red comunitaria.

Llegamos temprano y los vecinos tenían planificado un trabajo, siempre lo hacen en comunidad, iban buscar las vacas de una familia del paraje.

![](/assets/img/las_guindas/IMG_5400.JPG)

Entonces salimos a recorer la zona.

![](/assets/img/las_guindas/IMG_5408.JPG)
![](/assets/img/las_guindas/IMG_5411.JPG)

La zona es de sierra. También hay bosques de pinos. Todas cuestiones a tener en cuenta al momento de definir los lugares óptimos para montar torres con el objetivo de optimizar la cobertura.

Al regresar de nuestra recorrida los vecinos ya habían llegado, entonces nos pusimos a trabajar en un 1er relevamiento. Guillermo había llevado imagénes satelitáles impresas, donde los vecinos indicaron el lugar de sus casas, la escuela y puntos estratégicos para tener la mejor cobertura.

![](/assets/img/las_guindas/IMG_5423.JPG)
![](/assets/img/las_guindas/IMG_5424.JPG)
![](/assets/img/las_guindas/IMG_5416.JPG)

## Proveedores de Internet

- [Cooperativa eléctrica de Alpa Corral](http://coopalpacorral.com.ar/)
- Jose Pittaro - 358 420-2128 (ISP de Alpa Corral)
- Telecom 4G

La escuela rural tiene conexión a Internet provista por el gobierno provincial.

En la zona no hay acceso a la red eléctrica. Toda la instalación deberá ser alimentada mediante energía solar.

## Relevamiento

![](/assets/img/las_guindas/Las_Guindas-relevamiento.png)

[Proyecto en Google Earth](https://earth.google.com/earth/d/1YsoGfjdHXdezK_QvQFbUOS3nx3dkbW9d?usp=sharing)

## Conclusión
La recepción de la propuesta fue recibida de la mejor manera. Muchas familias están averiguando como contratar el servicio de Internet de manera individual, pero el alto costo que tiene imposibilita el acceso. Las familias son pequeños productores y carecen del recurso económico para hacer las instalaciones.

## Listado de Familias con las que nos reunimos

- Patricio Zabala
- Diego Nievas
- Ramon Gomez
- Fabian Zabala
- José Suarez
- Julio Romero

## Familias interesadas que no pudieron asistir

- Franco Oviedo
- Domingo Ponce
- Walter Romo
- Juan Manuel Schiavi
- Lisandro Schiavi
- Elba Arias
- Gladis Gonzales

## Observaciones

- No hay red eléctrica.
- Zona serrana.
- Comunidad solidamente constituida.
- Un joven integrante de la comunidad - Nico - trabajó un año haciendo instalaciones del servicio de Internet (coop Alpa Corral)
