---
title: Relevamiento en Las Guindas para instalar la torre
layout: default
hero_height: small
---

![](https://chocancharava.libre.org.ar/torre/photo1681213879.jpeg)
cerro donde se instalará la torre (vista desde la casa de la UNRC)

![](https://chocancharava.libre.org.ar/torre/photo1681213877.jpg)
cerro donde se instalará la torre (vista desde la casa de la UNRC)

![](https://chocancharava.libre.org.ar/torre/photo1681213878.jpeg)
casa de la UNRC

![](https://chocancharava.libre.org.ar/torre/1635355937223.jpg)
lugar donde colocar la torre

![](https://chocancharava.libre.org.ar/torre/1635355550148.jpg)
lugar donde colocar la torre
