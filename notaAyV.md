---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Nota al Decano de la Facultad de Agronomía y Veterinaria UNRC
subtitle: Chocancharava Libre<br>Red Comunitaria
layout: default
hero_height: small
---

Río Cuarto, 15 de junio de 2020

Al Decano de la facultad de Agronomía y Veterinaria

Prof.  Sergio González 

Universidad Nacional de Río Cuarto

S  /  D

De nuestra consideración:

Nos dirigimos a Uds. y/o por su intermedio a quien corresponda como referentes de Mesas territoriales y colectivo de Organizaciones e instituciones educativas locales a fin de solicitar colaboración y acompañamiento por parte de la Facultad de Agronomía y Veterinaria al proyecto "Plataforma educativa comunitaria", el cual brindará acceso mediante la red de telefonía celular SIN COSTO a contenidos y servicios orientados a estudiantes no universitarios de la región, atendiendo especialmente el contexto actual.

Para esto necesitamos disponer de un servidor y un sub dominio AyV.unrc.edu.ar para uso exclusivo de esta iniciativa.

Esta gestión nos permitirá resolver una serie de limitaciones que hoy condicionan altamente nuestras prácticas sociales y pedagógicas por las desigualdades existentes en el acceso y uso de la conectividad en los barrios populares.

Como todos sabemos, en tiempos de cuarentena la comunicación virtual es central para garantizar el derecho a la educación y dar continuidad a los procesos sociales, educativos y productivos que desarrollamos. La mayoría de nuestros estudiantes o grupos con los que trabajamos presentan serias dificultades para acceder a la conectividad por el costo económico que la misma implica, pero también presentan limitaciones en el acceso a dispositivos tecnológicos, conocimientos en su manejo, entre otros.

Comprometidos con esta problemática hemos buscado caminos alternativos que nos permitan resolver las limitaciones existentes. Entendemos que la universidad es un actor estratégico en el entramado que estamos construyendo ya que tiene las posibilidades de generar una serie de acciones que achicarán el margen de las desigualdades existentes. 

Sabiendo de su compromiso institucional ya que estamos articulando con el proyecto de Huertas y Mesas territoriales con su Facultad (por intermedio del Consejo social) solicitamos realice las gestiones pertinentes para conseguir las condiciones técnicas necesaria para avanzar en este desafío. En este marco, el dominio solicitado posibilitará el acceso gratuito desde el sistema de telefonía celular, lo que permitirá brindar servicios de libre acceso (web, mensajes, red social, etc) a estudiantes que no tienen wi-fi en sus domicilios, y que usan el teléfono celular como principal acceso a los contenidos de la escuela.

Comunicamos, además, que contamos con la asistencia técnica de Daniel Bellomo, trabajador de la faculta de Agronomía y Veterinaria de la UNRC.    

Sin otro particular y a la espera de una pronta y favorable respuesta, los saludamos muy fraternalmente.

- CENMA Remedios Escalada de San Martín Anexo Alberdi
- Escuela Paula Albarracín de Sarmiento anexo Delicias, Educación de jóvenes y Adultos
- Biblioteca Popular D. F. Sarmiento
- Sedronar
- Instituto Quechalen, Anexo Barrio Obrero
- CIC. Centro Integrador Comunitario
- EPyCA. Proyecto Salud Productiva
- CENPA 7
- CENMA BANDA NORTE
- CENPA 7 Extensión áulica
- Escuela Nocturna Paula Albarracín de Sarmiento Anexo Obrero
- CENMA RESMA ANEXO Obrero
- Escuela Olegario Victor Andrade, Colonia Santa Clara
- Otras…
