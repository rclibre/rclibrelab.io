---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Material educativo
subtitle: documentos, videos y artículos
layout: default
---

## Documentos
- [Declaración de la primera Cumbre Latinoamericana de Redes Comunitarias, septiembre de 2018](/docs/documento-final_CLRC-2018.pdf)

## Despliegue
- [Protocolo despliegue redes comunitarias en cuarentena](/docs/Protocolo.Despliegue.Redes.Comunitarias.en.Cuarentena.pdf)
- [web de Documentación en Altermundi](http://docs.altermundi.net)

## Reportes
- [Connecting the unconnected. Reporte de APC (en inglés)](https://www.apc.org/sites/default/files/APC_CR_CG_LocNet_Book_30042020-pages.pdf)

## Videos
- [Mapeo colaborativo de una Red Libre y Comunitaria de Internet](https://youtu.be/vDLYUZZEEyA)
- [LibreRouter: mesh technology with Community Networks](https://youtu.be/GP8dCTHTlW0)

### Semilleros
- [Participating in the Abya Yala's Seedbed of Community Networks in Cherán, Mexico](https://youtu.be/PyN8n94ROyE)
- [Semillero de Redes Comunitarias junto a la CTO Traslasierra](https://youtu.be/_EMmU707-v4)

### Encuentros
- [Encuentro Redes Comunitarias en Kenya 2019](https://youtu.be/1V9zYBKqlcQ)
- 2da Cumbre Latinoamericana de Redes Comunitarias (2019)
  - [Capitulo 1](https://www.youtube.com/watch?v=1Kgtfit8vkM)
  - [Capitulo 2](https://www.youtube.com/watch?v=lVpsLygJEbg)
- [Cumbre Latinoamericana de Redes Comunitarias (Argentina, 2018)](
https://www.youtube.com/playlist?list=PL_Dpv4cu0bApJXyFk0I1P6I28ucCsMZQW)
- [Redes comunitarias en Cordoba](
https://www.youtube.com/playlist?list=PL_Dpv4cu0bApIxSPxOyNsS7PTuHgKlR4y)

## Noticias
- [Redes libres comunitarias de Córdoba garantizan el derecho humano a Internet](https://lmdiario.com.ar/contenido/303384/redes-libres-comunitarias-de-cordoba-garantizan-el-derecho-humano-a-internet)
- [Cumbre Argentina de Redes Comunitarias. Internet desde los territorios (2019)](
https://altermundi.net/2020/01/18/cumbre-argentina-de-redes-comunitarias-internet-desde-los-territorios/)

## Investigación

- [AlterMundi y la primera red comunitaria de Internet cien por ciento LibreRouter y extendida durante la pandemia de COVID-19](http://democratizarcomunicacion.fcc.unc.edu.ar/wp-content/blogs.dir/27/files/sites/27/AlterMundi-y-la-primera-red-comunitaria-de-Internet-cien-por-ciento-LibreRouter-y-extendida-durante-la-pandemia-de-COVID-19.pdf)

## Bibliografía
- [Redes Comunitarias en América Latina. Desafíos, Regulaciones y Soluciones](https://www.internetsociety.org/es/resources/doc/2018/redes-comunitarias-en-america-latina)
