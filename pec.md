---
title: Plataforma Educativa Comunitaria
layout: default
hero_height: small
---

> Proyecto para ser presentado al Dir. del **Instituto de Investigaciones Sociales, Territoriales y Educativas** (ISTE), Lic. Edgardo Carniglia.

## Objetivo

Brindar acceso mediante la red de telefonía celular **sin costo** (cero rating) a contenidos y servicios orientados a estudiantes no universitarios, atendiendo especialmente el contexto de aislamiento social preventivo y obligatorio, planificando transitar un proceso a largo plazo.

## Propuesta

Este proyecto nos permitirá resolver una serie de limitaciones que hoy condicionan altamente nuestras prácticas sociales y pedagógicas por las desigualdades existentes en el acceso y uso de la conectividad en los **barrios populares y zonas rurales**.

En tiempos de cuarentena el acceso Internet es central para garantizar el derecho a la educación y dar continuidad a los procesos sociales, educativos y productivos que desarrollamos. La mayoría de nuestros estudiantes o grupos con los que trabajamos presentan serias dificultades para acceder a la conectividad por el costo económico que la misma implica, pero también presentan limitaciones en el acceso a dispositivos tecnológicos, conocimientos en su manejo, entre otros.

Comprometidos con esta problemática hemos buscado caminos alternativos que nos permitan resolver las limitaciones existentes. Entendemos que el **ISTE** es un actor estratégico en el entramado que estamos construyendo ya que tiene las posibilidades de generar una serie de acciones que achicarán el margen de las desigualdades existentes.

La propuesta contempla poner en servicio una serie de aplicaciones - usando Software Libre -, que **se podrán acceder sin costo mediante telefonía celular** (cero rating), entre las que se encuentran: **sitio web, aula virtual, wiki, mensajería instantánea, red social, radio online, podcast, wikipedia, compartir archivos** y otros servicios orientados a potenciar la comunicación y dar más herramientas a docentes y estudiantes.

Adicionalmente a lo coyuntural (el contexto de pandemia), la invitación es transitar un camino de **Soberanía Tecnológica** y **construcción horizontal**, donde priman **valores éticos en el manejo de los datos** y la **colaboración**, para permitirnos deconstruir usos y costumbres que hacen a la vinculación mediante Internet de la comunidad educativa. Resaltando el efecto multiplicador de una acción de este tipo generada desde la escuela.

## Integrantes del colectivo que desarrolla la propuesta

- María Cecilia Baigorria. EPyCA Río Cuarto. Escuela Quechalén.
- Graciela Sack. P. U. C.E. Escuela Antonio Sobral, Cuatro Vientos.
- Agustina Bovetti. Escuela rural Olegario Victor Andrade. Alejandro Roca.
- Nicolas Bustos. CENMA Banda Norte anexo barrio IPV.
- Patricia Basqueto. Escuela Cnel. Antonino Baigorria, Villa Sta Eugenia, Alpa Corral.
- Anabella Echenique. C.E. Escuela Dr. Rodolfo de Ferrari Rueda. Río Seco.
- Liliana Tamios. C.E. Escuela Manuela Guillermina Maldonado de Partelli. La Esquina.
- Ana. C.E. Escuela San Bartolomé. San Bartolomé.
- Sandra Tamiozzo. C.E. Escuela San Isidro Labrador. Campo La Aguada.
- Gabriela Santos. Escuela Eutaquia Valentina Becerra de Echenique. Las Tapias.
- Magaly Villarroel. C.E. Escuela Capitán Martín de Alva. Paso del Durazno.
- Nancy Bonotto. C.E. Escuela Remedios Escalada de San Martín. San Bernardo.
- Nancy Lopez. C.E. Escuela José de San Martín. Las Cañitas.
- Julieta Mina. C.E. Escuela San Juan Bosco de San Ambrosio.
- Alejandra Villamador. Escuela José de San Martín. La Aguada.
- Carlos Jaime, Escuela José Hernandez, Villa El Chacay.
- Ana Felippa.
- Ana Bruna, Directora IPEM 135 José Felix Recalde Sarmiento. Nono.
- Franco Bellomo. Manas Tech.
- Aldana D'Andrea, UNRC, Dpto. de Filosofía.
- Emiliano Campoamor, UNRC, Dpto. de Filosofía.
- Juan Carmelo Valmala. Profesor IPEM 135, José Felix Recalde Sarmiento Nono.
- Mónica Castro, CENMA N°73 Anexo Ciudad Nueva.
- Rubén Enrique Ramírez, Coordinador CENMA 73, "Dr, Arturo Jauretche" Sede Achiras.
- Graciela Carrara, CENMA Antonio Sobral, Laboulaye.
- Monica Bridarolli, CENMA 202, sede Sampacho y Bulnes
- Daniel Bellomo. Chocancharava Libre.
- y más ...

## Instituciones que integran la propuesta

- [AlterMundi](https://altermundi.net)
- **Escuela Quechalen** Anexo Sagrada Familia, Anibal Ponce 1262, Barrio Obrero.
- **CENMA Remedios Escalada de San Martín** anexo Alberdi, Entre Ríos y Anchorena, Barrio Alberdi.
- **Escuela Paula Albarracin de Sarmiento** anexo Delicias, Quirico Porreca 1310, Barrio Alberdi.
- **Biblioteca Popular D. F. Sarmiento**, Belisario Roldán 201, Barrio Alberdi.
- **Escuela Nocturna Paula Albarracín de Sarmiento** Anexo Obrero, Adelia María y Malabia, Barrio Obrero.
- **CENMA Resma** anexo Obrero, Adelia María y Malabia, Barrio Obrero.
- **CENPA 7**.
- **CENPA Banda Norte**.
- **Escuela Olegario Victor Andrade**, Colonia Santa Clara.
- **CENMA Banda**, Norte anexo barrio IPV.
- **CENMA N° 73**, Dr, Arturo Jauretche" Sede Achiras.
- **CENMA N° 24** Sede Municipalidad de Río Cuarto.
- **CENMA Antonio Sobral**, Laboulaye.
- **CENMA 202**, sede Bulnes.
- **CENMA 202**, sede Sampacho.
- y más ...

## Adherentes

- [Instituto de Tecnología Socioambiental](https://www.itsocioambiental.org)
- Mauricio José Vigliocco.
- Asociación Civil La Vaca.
- Grupo de Genética y Mutagénesis Ambiental - UNRC
- Asamblea Río Cuarto sin agrotoxicos.
- Cátedra Libre de Soberanía Alimentaria Río Cuarto.
- Marcela Casas (docente).
- Red Comunitaria El Valle Reinicia, Nono.
- Comunidad Trabajo y Organización Traslasierra (CTO).
- Unión de Trabajadores Rurales (UTR - Córdoba)
- y más ...

### Noticias

[Impulsan una web de conexión gratuita para alumnos primarios y secundarios](https://www.puntal.com.ar/conectividad/impulsan-una-web-conexion-gratuita-alumnos-primarios-y-secundarios-n111957)

[Le solicitarán a la UNRC el dominio institucional .edu.ar para desplegar una red comunitaria](https://soundcloud.com/unrcradio/le-solicitaran-a-la-unrc-el-dominio-institucional-eduar-para-desplegar-una-red-comunitaria)

[Promueven el uso gratuito de telefonía celular para alumnos de escuelas](https://www.lv16.com.ar/sg/nota/138229/promueven-el-uso-gratuito-de-telefonia-celular-para-alumnos-de-escuelas-perifericas-de-la-ciudad)

### Referencias

[Rating cero para Seguimos Educando, app Cuidar - Covid-19 y Universidades nacionales](https://www.argentina.gob.ar/jefatura/innovacion-publica/acciones-coronavirus/acuerdos-con-empresas-de-telecomunicaciones)

[El Ministerio de Educación de la Nación y el ENACOM acuerdan el acceso gratuito a las plataformas educativas desde los celulares](https://www.argentina.gob.ar/noticias/el-ministerio-de-educacion-de-la-nacion-y-el-enacom-acuerdan-el-acceso-gratuito-las)

[Estudiantes universitarios podrán acceder a distintas plataformas en forma gratuita](https://www.noticiasde.com.ar/sociedad/estudiantes-universitarios-podran-acceder-a-distintas-plataformas-en-forma-gratuita-124084)

[Instituto de Investigaciones Sociales, Territoriales y Educativas](https://www.hum.unrc.edu.ar/edgardo-carniglia-es-el-director-regular-del-instituto-de-investigaciones-sociales-territoriales-y-educativas-iste/)