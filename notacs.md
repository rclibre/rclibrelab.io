---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Nota al Consejo Social de la UNRC
subtitle: Chocancharava Libre<br>Red Comunitaria
layout: default
hero_height: small
---

Río Cuarto, 21 de mayo de 2020

A los coordinadores del Consejo Social

Universidad Nacional de Río Cuarto

De nuestra consideración:

Nos dirigimos a UDs y/o por su intermedio a quien corresponda como referentes de Mesas territoriales y colectivo de Organizaciones e instituciones educativas locales a fin de solicitar ante las autoridades institucionales pertinentes, en el marco de la colaboración de la universidad con extender los espacios educativos, la gestión para disponer de una maquina virtual en la RIU y un sub dominio unrc.edu.ar para uso exclusivo de esta iniciativa.

Esta gestión nos permitirá resolver una serie de limitaciones que hoy condicionan altamente nuestras prácticas sociales y pedagógicas por las desigualdades existentes en el acceso y uso de la conectividad en los barrios populares.

Como todos sabemos, en tiempos de cuarentena la comunicación virtual escentral para garantizar el derecho a la educación y dar continuidad a los procesos sociales, educativos y productivos que desarrollamos. La mayoría de nuestros estudiantes o grupos con los que trabajamos presentan serias dificultades paraacceder a la conectividad por el costo económico que la misma implica, pero también presentan limitaciones en el acceso a dispositivos tecnológicos, conocimientos en su manejo, entre otros.

Comprometidos conesta problemática hemos buscado caminos alternativos que nos permitan resolverla misma. Entendemos que la universidad es un actor estratégico en el entramadoque estamos construyendo ya que tiene las posibilidades de generar una serie deacciones que achicarán el margen de las desigualdades existentes. Sabiendo de su compromiso con los que menos tienen, solicitamos realice las gestiones pertinentes para conseguir las condiciones técnicas necesaria para avanzar eneste desafío. En este marco, el dominio solicitado posibilitará el acceso gratuito desde el sistema de telefonía celular, lo que permitirá brindar servicios de libre acceso (web, mensajes, red social, etc) a estudiantes que notienen wi-fi en sus domicilios, y que usan el teléfono celular como principal acceso a los contenidos de la escuela.

Sin otro particular y a la espera de una pronta y favorable respuesta,los saludamos muy fraternalmente.

- CENMA Remedios Escalada de San Martín Anexo Alberdi
- Escuela Paula Albarracin de Sarmiento anexo Delicias, Educación de jóvenes y Adultos
- Biblioteca Popular D. F. Sarmiento
- Sedronar
- Instituto Quechalen, Anexo Barrio Obrero
- CIC Centro Integrador Comunitario
- EPyCA Proyecto Salud Productiva
- CENPA 7
- CENMA BANDA NORTE
- CENPA 7 Extensión aulica
- Escuela Nocturna Paula Albarracín de Sarmiento Anexo Obrero
- CENMA RESMA ANEXO Obrero
