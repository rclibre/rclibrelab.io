---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: Redes Comunitarias
subtitle: Soberanía Tecnológica
layout: default
---

- Escuela de Educación Secundaria Modalidad Técnico Profesional N° 486 "Francisco Netri" - Carcarañá - Santa Fe
- Proyecto de vinculación tecnológica: vinculación entre actores del sistema agroalimentario porcino mediante el Centro de Información de Actividades Porcinas (CIAP).
  - Directora: Patricia Silva
  - Co-directora: Patricia Skejich

# Redes libres, comunitarias y descentralizadas

- [Nuestro pedacito de Internet](https://altermundi.net/documentacion/redes-libres-comunitarias-y-descentralizadas/)

![](https://altermundi.net/wp-content/uploads/2021/01/infografia.jpg)

![](https://www.redeszone.net/app/uploads-redeszone.net/2018/12/LibreRouter.jpg)

![](https://altermundi.net/wp-content/uploads/2020/10/Puertos-WAN-y-LAN.png)

![](https://i1.wp.com/www.redeszone.net/app/uploads/2018/12/Red-Mesh-LibreRouter.png)

![](https://i.ytimg.com/vi/Lzz4OrCpiTQ/maxresdefault.jpg)

# Plano con el diseño propuesto

![](/assets/img/esc_486_Netri/diagrama_de_la_red_esc_486_Netri.jpg)

# Fotos de la juntada (05/07/21)

![](/assets/img/esc_486_Netri/image_juntada_01_05-07-21.jpeg)

![](/assets/img/esc_486_Netri/image_juntada_02_05-07-21.jpeg)

![](/assets/img/esc_486_Netri/jornada_de_vinculacion_1.jpg)

![](https://altermundi.net/wp-content/uploads/2020/10/logo-animado-fondo-blanco1.gif){: .center-image }

# Videos
- [AlterMundi](https://youtu.be/iAn1pRQwRoA)
- [Primeros Pasos Con LibreRouter](https://youtu.be/gF2zUIKNXLc)
- [Cumbre Latinoamericana de Redes Comunitarias - 2018](https://youtu.be/sbjFoDgAuko)
- [Semillero de Redes Comunitarias junto a la CTO Traslasierra](https://youtu.be/_EMmU707-v4)
- [Dossier QuintanaLibre](https://youtu.be/lgSrN_Xb8cM)
- [Dossier NonoLibre](https://youtu.be/IuBVI5ArrXU)
- [Dossier LaSerranitaLibre](https://youtu.be/ruj_1PpH29M)
- [Encuentro Redes Comunitarias en Kenya 2019](https://youtu.be/1V9zYBKqlcQ)
- [Semillero de Redes Comunitarias en Cherán, Mexico](https://youtu.be/PyN8n94ROyE)
- [Mapeo colaborativo de una Red Libre y Comunitaria de Internet](https://youtu.be/vDLYUZZEEyA)
- [Montaje en pared de un nodo LibreRouter](https://youtu.be/c-VX6YRBVLY)
- [Comunidades haciendo Internet- Experiencia QuintanaLibre](https://youtu.be/DcOAePVwafs)

# Documentación
- [https://docs.altermundi.net](https://docs.altermundi.net)

