---
title: Relevamiento en el Refugio Libertad
layout: default
hero_height: small
---

# Relevamiento en el Refugio Libertad por parte de integrantes de la cátedra de Espacios Verdes (Facultad de Agronomía y Veterinaria) y del Grupo de Energia Solar (Facultad de Ingenieria)

![](https://i.imgur.com/jI7PmEj.jpg)

Con el objetivo de planificar el ordenamiento de los espacios verdes y el diseño de un datacenter en el Refugio Libertad, el jueves 30 de marzo de 2023, integrantes de la Cátedra de Espacios Verdes (Facultad de Agronomía y Veterinaria) y del Grupo de Energia Solar (Facultad de Ingenieria) de la Universidad Nacional de Río Cuarto visitaron el predio. 

![](https://i.imgur.com/ioIs8BM.jpg)

![](https://i.imgur.com/bidzDDA.jpg)

Allí se realizó un relevamiento, observando las construcciones y la vegetación existente.
![](https://i.imgur.com/hvdgbzZ.jpg)

![](https://i.imgur.com/F4r8kSl.jpg)

![](https://i.imgur.com/47wtfTf.jpg)

La arquitecta y Coordinadora técnica, Mariel Ivon, brindó la información acerca del uso y función que se le dará a futuro a cada sector del predio. Esto permitirá arribar a un ordenamiento de los diferentes espacios que lo componen, a través de la vegetación, priorizando el uso de especies nativas.

![](https://i.imgur.com/521r9ij.jpg)

![](https://i.imgur.com/qnZfoZz.jpg)

Por otro lado, se evaluó la posibilidad de utilizar una cisterna semienterrada actualmente en desuso para ubicar equipos informáticos (datacenter). Se propone complementar esta estrategia con una cubierta verde para aprovechar este tipo de estrategias bioclimáticas que permiten reducir el uso de energía convencional en la climatización del espacio destinado al datacenter.

![](https://i.imgur.com/KY9jXB8.jpg)

![](https://i.imgur.com/FzE7Ksy.jpg)

![](https://i.imgur.com/PZFjvnf.jpg)

![](https://i.imgur.com/8uvkDmP.jpg)

![](https://i.imgur.com/UNCelcL.jpg)

La actividad está enmarcada en el convenio de colaboración entre el Refugio Libertad y la UNRC (RES. Nº 177 / 07-06-22 C.S - expediente nro 138527).

Refugio Libertad: https://refugio.libre.org.ar
