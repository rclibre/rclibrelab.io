Comunicación con los estudiantes.

Objetivo:

- Optimizar la comunicación con los estudiantes.

Escenarios actuales:

1. Estudiante tiene internet (banda ancha: wi-fi, cable, ADSL, etc) en su casa.
   En este caso la comunicación escuela - estudiante funciona correctamente.

2. Estudiante accede a internet mediante un plan de datos de la empresa de telefonía celular .
   El sistema de datos pregago es extremadamente caro, lo que lleva que no lo tengan disponible frecuentemente.

3. Estudiante no tiene acceso a internet (no tiene banda ancha ni plan de datos).

Los escenarios 2 y 3 son los más frecuentes, por lo tanto cuales nos vamos a enfocar.

¿Cómo nos comunicamos con los estudiantes? ¿Qué aplicaciones usamos?

Las aplicaciones más usadas, casi con exclusividad, son WhatsApp y Face. Es decir, usamos "lo dado" (impuesto por El Sistema). Estas aplicaciones resultan muy sencillas de usar, vienen pre instaladas en los celulares y se convirtieron en estándares *de facto*.
