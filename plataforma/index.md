---
title: <br><br>Plataforma educativa comunitaria
author: <br><br>Propuestas<br><br>14/05/2020
output: revealjs::revealjs_presentation
---

# Contexto

- cuarentena
- pandemia
- comunicación con estudiantes

# mensajes

- [briar](https://briarproject.org)
- [delta chat](https://delta.chat)
- Jabber

# web

- [dokuwiki](https://www.dokuwiki.org)
- biblioteca
- Wikipedia

# compartir archivos

- torrent
- [TrebleShot](https://github.com/genonbeta/TrebleShot)
- [ShareToComputer](https://github.com/jimmod/ShareToComputer)

# reuniones (voz)

- [mumble](https://www.mumble.info)

# red social

- [Mastodon](https://mastodon.social)

# audios

- radio
- podcast

# documentos compartidos

- [pad](https://pad.riseup.net)

# aula virtual simple

- el alumno no se loguea
- diseñado p/ el cel
- óptimo uso del ancho de banda
- diseño minimalista
